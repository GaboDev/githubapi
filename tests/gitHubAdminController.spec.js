describe("gitHubAdminController test", function () {

    let githubControllerScope, GithubService;

    beforeEach(function () {
        GithubService = jasmine.createSpyObj('GithubService', ['getProjectsByName']);
        module('app');

        inject(function ($rootScope, $controller) {
            githubControllerScope = $rootScope.$new();
            $controller('githubAdminController', {
                $scope: githubControllerScope,
                GithubService: GithubService
            })
        });
    });

    it('load projects onFailure with null project name', inject(function ($q) {
        let projectNameToSearch = null;
        GithubService.getProjectsByName.and.returnValue($q.when("response"));
        githubControllerScope.projectName = projectNameToSearch;
        githubControllerScope.search();
        githubControllerScope.$digest();
        expect(githubControllerScope.projects).toEqual([]);
    }));

});