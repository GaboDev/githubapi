app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise("/");

    let githubAdmin = {
        name: '/',
        url: '/',
        component: 'githubAdmin'
    };

    $stateProvider.state(githubAdmin);

}]);