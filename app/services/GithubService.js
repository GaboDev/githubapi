app.factory('GithubService', ['$http', function ($http) {

    const endpoint = "https://api.github.com";

    return {
        getProjectsByName: function (projectName) {
            return $http.get(endpoint + "/search/repositories?q=" + projectName).then(function (response) {
                return response.data;
            }, function (error) {
                return error;
            });
        },
        getComments: function (repoUrl) {
            return $http.get(endpoint + repoUrl);
        }
    }
}]);