'use strict';

app.controller('githubAdminController', ['$scope', 'GithubService', function ($scope, GithubService) {

    $scope.message = null;
    $scope.loadingData = false;
    $scope.projectName = null;
    $scope.projects = [];
    $scope.selectedProject = null;
    $scope.httpResult = false;


    $scope.search = function () {

        if ($scope.projectName !== null && $scope.projectName.length > 0 && !$scope.loadingData){

            $scope.loadingData = true;
            $scope.message = null;
            $scope.projects = [];
            $scope.selectedProject = null;

            GithubService.getProjectsByName($scope.projectName).then(function (response) {
                if (response.total_count > 0){
                    $scope.projects = response.items;
                    $scope.httpResult = true;
                }else {
                    $scope.message = "There were no results.";
                    $scope.httpResult = false;
                }
                $scope.loadingData = false;

            }, function (error) {
                $scope.loadingData = false;
                $scope.httpResult = false;
                $scope.message = "There was an unexpected error.";
            });

        }

    };


}]);

app.component('githubAdmin', {
    templateUrl: "views/githubAdmin/githubAdmin.html",
    controller: 'githubAdminController'
});