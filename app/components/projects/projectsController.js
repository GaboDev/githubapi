'use strict';

app.controller('projectsListController', ['$scope', function ($scope) {

    $scope.selectedProject = null;

    $scope.showInfo = function (project) {
        $scope.selectedProject = project;
    };

}]);

app.component('projectsList', {
    bindings: {
        projects: "="
    },
    templateUrl: 'views/projects/projects.html',
    controller: 'projectsListController'
});