let gulp        = require('gulp');
let browserSync = require('browser-sync').create();
let concat = require('gulp-concat');

let nodeModulesImports = [
    'node_modules/angular/angular.js',
    'node_modules/@uirouter/angularjs/release/angular-ui-router.min.js'
];
let jsFiles = ['app/*.js', 'app/*/*.js', 'app/*/*/*.js'];
const jsImports = nodeModulesImports.concat(jsFiles);
const htmlFiles = ['app/components/*.html', 'app/components/*/*.html', 'app/components/*/*/*.html'];

gulp.task('js', function () {
    return gulp.src(jsImports)
        .pipe(concat('app.js'))
        .pipe(gulp.dest('dist/js'));
});

gulp.task('images', function () {
    return gulp.src('app/assets/images/*.*')
        .pipe(gulp.dest('dist/images'));
});

gulp.task('views', function () {
    return gulp.src(htmlFiles)
        .pipe(gulp.dest('dist/views'));
});

gulp.task('css', function () {
    return gulp.src('app/components/*/*.css')
        .pipe(concat('app.css'))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('html', function () {
    return gulp.src('app/index.html')
        .pipe(gulp.dest('dist/'));
});

gulp.task('watch', gulp.series(['js','views', 'css', 'html', 'images'], function (done) {
    browserSync.reload();
    done();
}));


gulp.task('default', gulp.series(['js','views', 'css', 'html', 'images'], function () {

    // Serve files from the root of this project
    browserSync.init({
        server: {
            baseDir: "dist/"
        }
    });

    // add browserSync.reload to the tasks array to make
    // all browsers reload after tasks are complete.
    gulp.watch(["app/*/*.*","app/*.*","app/*/*/*.*"], gulp.series('watch'));
}));